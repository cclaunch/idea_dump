General Apps
============

Super generic list of mobile apps, PC apps, or devices

* Clothing Size Tracker - Keep track of family / friends sizes; Share with others.; Have a range of sizes. ;T-shirt size different from blouse, etc.
* Headache Forecaster - Using sinus / allergy / etc to forecast.; Auto add to calendar
* Medical History - Keep track of doc visits, meds, etc; Reminder to take meds
* Phone As Mouse
* Prayer - Integrate with Facebook; Prayer reminders; Share with others; Anonymous? You were just prayed for
* Teacher Helper - Parent information.; Keep track of kids.; Discipline tracker
* Time Budget - Mint.com for time
* Timer - Tons of options; Sounds; Every X minutes
* Car maintenance -
* Drag Racing Christmas Tree - Have ability to keep averages, possibly compete with others?
* Weight tracker
* School metrics app windows - Didn't answer question, handle grading / metrics for that.; Make for Mac too?
* Gate Code App - Store gate codes for contacts ; Somehow be triggered by gps coords?; Quick and dumb;  Have it save as part of the contact? depends on phone/gmail?; display right on screen; easy to SMS gate code to someone
* Cable Planner App - plan a new home theater or network setup, keep list of canned components and their connection types. helps know cable run types and lengths
* Phone auto answer - When you're cooking / shower / etc and too messy to answer phone, set auto answer to specific people
* Location service using wifi APs within a store
* Number picker for dirty santa games
* Favorites for stuff , food, etc
* Extrapolate clothing sizes from photos -
* Gas mileage tracker widget to google spreadsheet -
* Spice tracker -
* Quick record (audio) - With auto-upload? every x seconds/minutes?; Google drive?
* Long range weather accuracy tracker - Keep track of long term weather forecast and compare/store data with actuals as they happen.
* Poo tracker - Track when you last pooed!
* Front end for PhotoRenamer -
* App/webpage to track average home value - Do this manually for now?
* Home value app - Given a zip code / area / etc, what's the average home value. Use HAR?
* Nice (tablet size?) car app for OBD -
* Aeropress timer/recipe app -
* App to use phone for external display for games -
* Kids learning apps for android tablets - Needs difficult exit (ignore home buttons, etc)
* House log - routine maintenance reminders
* Sound manager -
* Phone scale, use touch screen pressure -
* Battery tester for vehicles -
* Non scrolling photo album - some folks have motion sickness w/scrolling
* Keep track of online street cred for various services -
* Nice cross platform animated app UI -
* Rolling contact photos -
* WiFi people detection -
* NFL playoff app - calculate possibilities within a few weeks out of playoffs - https://gitlab.com/cclaunch/playoffs
* Port DOS games to android? -
* Organize photos by topic? -
* Avocado reminder -
* Computer app to send notifications to Android wear -
* Plant Tracker (who gave me that plant?) -
* Star Trek style watch face -
* Generic card game app - Use bluetooth for close area games? ; Users can create their own games with regular cards? Ability to upload/create other cards?; Share other cards?
* Rename pdf files based on titles? -
* Instant record button for phone -
* Track people via their trackers -
* Tournmanet bracket creator / printer / maker - for various tournament types (target ncaa), allow easy ingestion of paper brackets, or web app for easy entering by tournmanet participants
* Programmatically monitor CPU usage/mem/temp/etc + UPS battery stats/etc -
* Game for showing how a govt /city could operate w/o taxes - good for kids learning
* Guitar distortion pedal -
* Pollen forecaster / headache / etc -
* Read clash of clans data -
* Changelog for installed apps -
* Bluetooth indicator for phone calls in loud environment - e.g. auto shop
* Keyboard for watch (quadrants?) -
* Bluetooth garage opener? -
* Buzz in for games with multiple phones -
* Store hours, garbage days, etc -
* Use old photo frame as front door answering -
* Poker tournament app - Keep track of blinds;  Keep track of buy ins, rebuys, etc; Auto produce payout list (including options like rebuys to winner, minimum players paid out, and both set and calculated payouts); Presets for stuff like WSOP;  Send text when blinds are up; IFTT incorporation?; Chip counts;  Scheduled breaks; Settable warnings for events;  Table/seat selector
* brightness profile for windows - allow user to sit down and choose their profile easily (good for shared PCs)
* Add aches and pains weather predictor - Let user enter in days when they have specific symptoms, app records weather, then begins to make correlations
* Wheel of Fortune cross off letters - while watching show
* Students attention app plus one when interacting -
* Make network tester out of two arduinos -
* Call your own phone app for Android wear -
* Watch app to record conversations - without needing phone connected?
* Plant watering app (keep track of when you watered) -
* Weather man tracker -
* App that shows companies and their parent companies - tree format all the way to the top
* Keep track of meds in cabinet with expiration dates -
* Turning dash cam - PTZ in direction you're steering
* Easy book wish list search -
* Figure out what plane just flew overhead (call it "SpyPlane") - use flight data online
* See through a fence (by translating slowly with camera and stacking pictures) -
* Family stories sharing - record older folks stories for permanent keeping; video/audio/text
* Auto splice football games using field graphics -
* ETA send via text or email, etc (from location to person's address) - Watch app?
* Achievement recording system for classroom -
* website pinger (to check for changes) - Do some kind of website diff, set interval, notification options, etc
* 3d / depth camera for backing up trucks -
* Pet microchip reader? -
* Watch app to ring phone -
* Hand and foot card game -
* Ui to show what happened in a car wreck - simple graphics to storyboard the event
* Create a pi/arduino based smoker / bbq temperature solution and sell (could also work for coffee roasting?) -
* Dice app, save rolls by player name - other stats?
* generate list of famous movie scene film locations and combine with google maps -
* App to easily copy photos from phone to computer,,Windows app + phone app? (without cloud)
* Game which feeds off of major dynamic web sites, facebook/twitter/instagram/etc,,
* Background cutter application, automatically cut large images to account for varying screen/bezel sizes
* Medical app - symptoms tracker - e.g. "stomach started hurting at HH:mm mm/dd/yyyy" -
* Make a good backup system - not just files/photos, but config of phones, computers, devices, network, etc.  use checksums to detect duplicate photos/videos
* Voting tracker - keep track of people / things I vote for. could include commentary on reasoning, and statistics
* Scanner - simpler app for scanning directly to pdf.  linux?  https://github.com/fstromback/hp-scan
* PC / electronics parts inventory - PC parts, electronics, cables, etc.  - easy web app for keeping track.  keep database of what hardware is inside a particular PC (pc part picker downloads?)
* Homelab (/r/homelab) style app which is 3d and can "drill down" into computers/routers/equipment and show any data that device is capable of (temps, voltages, fan speeds, CPU usage, network usage, etc).  netdata might be useful as a provider.
* Windows based app which would synchronize settings for common apps (like sublime text, backgrounds, environment variables, etc)
* App for mocking a house purchase - include costs for distance from work/others, yard maintenance, estimate power usage, taxes, insurance, payments, payoff time
