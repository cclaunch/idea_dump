Baconfest
=========

An epic food gathering

Food list
---------

* Bacon explosion
* Bacon cookies
* Bacon wrapped
    * Jalapenos
    * Dates
    * Asparagus
    * Water chestnuts
* Bacon beer? Voodoo used to have one. Surely there’s something out there.
* Bacorn (bacon wrapped corn on the cob)
* Chicken fried bacon (very thin bacon needed)
* Candied bacon bourbon mixed drinks
* Baked brown sugar bacon
* Kettle maple bacon chips
