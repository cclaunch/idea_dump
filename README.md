The Idea Dump
=============

Welcome to the idea dump!  This repository was built with the goal of sharing ideas for software and/or maker  projects (though ideas need not be limited to those types).  The initial seeding of the ideas came from @cclaunch after realization that ideas came faster than time to work on them (got kids?).

Organization
------------

The ideas are organized into major top level categories and then each idea has its own folder with at the very least, a `README.md` file.  

Contributions
-------------

The ideas in this repository are 100% freely available, but if you decide to implement one of them, it'd be great to put a link to your implementation in that idea's `README.md` and perhaps a link in your repository back to this one.  It's highly encouraged to include as much detail as possible, including drawings, source code, BOMs (bill of materials), and anything else that helps capture the idea.
