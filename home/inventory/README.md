Home Inventory Tracking System
==============================

Good for insurance purposes or lending things

Items to Track
--------------

* DVD/Blurays - DVDProfiler is good, can we import this?
* CDs - Just upload them all to Google?
* Board Games - boardgamegeek.com has a way to enter them
* Tools/Electronics - keep model/serial number/photos
* Furniture/Appliances - keep model/serial number/photos
* Jewlery
* Clothes
* Guns
* Collectibles
* Games
* Major house items - HVAC, water heater, electrical panel, etc

Aspects to Track
----------------

* Model number
* Serial number
* Photo
* Purchase date
* Purchase price
* Purchase location
* Get rid of date (retire the item?) (sold price?)
* Borrowed/lent - to whom
* Movies/CDs/Board games - title, barcode, formats (BD/DVD/4K/3D/etc)
* Board games, record number of players, ages, time (check boardgamegeek for more)
* Jewelry - identifying marks
* Updateable replacement value (automatic would be sweet)

Tools to Help
-------------

* Accept barcode scanning for media
* Electronics or other items, scraper to look for firmware updates/recalls?
* Web app for easy editing
