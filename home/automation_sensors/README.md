Home Automation & Sensors
=========================

Big list of wants for home automation and sensing


Data Sources / Hardware
-----------------------

* [Ruuvi tags](https://ruuvi.com/) - BLE sensor, runs for years on a coin cell, great for getting temp/humidity/pressure/etc easily (not cheap though)
* Combine ruuvi tags with [Node Red plugin](https://github.com/ojousima/node-red) on Raspberry Pi 3
* ESP8266 based boards
* Arduino + Ethernet (some PoE?)
* Anything with a battery, monitor battery voltage if possible (ruuvi tags have it built in)
* Anything plugged in, include id of what house circuit it's on (traceability of failed/tripped circuits)
* [Sonoff](https://sonoff.itead.cc/en/) ridiculously cheap wifi relay board with built in support for google home
* [Aeris Neo SIM](https://neo.aeris.com/) - crazy cheap data only or SMS sim plans ($1/mo for 750KB of LTE)
* Influx Db - excellent time series database
* PostgreSQL or other relational database for device states/config?
* [SuperHouseTV Youtube channel](https://www.youtube.com/channel/UC75HTMhqVZs0sPOMTMQqI9g) - this guy has done some cool stuff


House items either completed or in work
---------------------------------------

* HVAC - instrument with ruuvi tags at intake and exhaust to constantly measure coil performance, smart thermostat (duh), measure current/voltage, notifications on/off/temp not keeping up
* Cameras - IP cameras - Blue Iris - great IP camera server with motion detection - detect motion notifications.  notifications when cameras are lost. tie in google home commands "show me drivway cam" etc
* Fridge - ruuvi tags in fridge and freezer, use humidity spikes to determine door open, notifications if door left open or temperature out of range
* Garage doors - relay board to engage physical switch terminals, use digital i/o (arduino) to read the two end of travel microswitches (measured at 8V on my garage door).  combination of these two microswitches should give door status:  open, partial, closed.  make sure device is wired to ensure reliability
* Tie in pluggable devices (lamps, x-mas tree, etc) with google home (many vendors with cheap solutions that tie to google home)
* [Google Home Notifier](https://github.com/harperreed/google-home-notifier-python) - awesome little script allowing you to tie into your google home and have it play custom sounds or say custom things (Fun for freaking wife out with random "Hello Sarah, you look nice today").


Future house item ideas
-----------------------

* Washer/dryer - vibration sensor?  or direct tie in to read current draw?  nofitications on start/stop and track time
* Mailbox - door open/close detect w/wifi or blueooth (will it reach?) - some sort of circuit which activates on a momentary, sends message (with ack) then shuts back off until opened again?  something to snap photo of person who opened? - pretty cool trigger board [here](http://www.kevindarrah.com/wiki/index.php?title=TrigBoard)
* Oven - thermocouple attached to arduino for temperature monitoring, more accurate than crappy oven thermometer.  notifications on temperature preheat reached (and stable).  easy display on phone? or even 7-segment display near oven?
* Dishwasher - vibration sensor or temperature sensor to detect cycle start/stop dry cycle completed?
* Doors/windows - reed switches hooked up to arduino, or COTS sensors? notifications on open/close/left open
* Could dumb door locks still be read for status?
* Network - glean data from router regarding outbound connectivity (pings, bandwidth tests), notifications of new devices, wifi scans for devices even not on local LAN?
* LIDAR or other range finding sensors for approach to doors, something to know the difference between a person and a car
* Read electric/water/gas meters with SDR (see sdr section of this repository)
* Water heater - thermocouple into the tank to read temperature and something to read water level?  Notifications on hot water running low
* Computer screens - write scripts which tie into IFTTT and google home to bring up various cameras/dashboards on the computer screen
* Tie in more lamps as well as built in light fixtures and fans (many available vendors or use sonoffs), important that physical switches still work!
* Plumbing - temperature sensors on pipes in the attic to detect temperature change, therefore water flow?  leak detect sensors near toilets/water heater/dishwasher/WD
* Central server to aggregate all data, could also include maintenance items with notifications such as air/water filter replacements, etc
* Espresso machine - plumb with water filter, instrument with temperature sensors and relays for on/off automatically or by remote commands
* Weather station for backyard - contribute to public websites?
* Bluetooth door locks?  Trustworthy or no?
* Separate monitor near TV or in kitchen that constantly displays the house UI
* Face recognition, person count on front door camera + alert (audible "3 unknown people at front door" or "Bob Jones is at the front door")
* grafana style dashboards could include health data (weight / calories / workouts/ time), budget data
* Portable "spot cams" that can easily be placed around the house during vacations / play time w/kids. raspberry pi?
* Use video analysis algorithm to determine if kid is asleep, then use that flag to set stuff like volume on tv, doorbell volume, google home volume, etc
* Use lightning sensor to determine threat of power loss, orderly shutdown of computers to save UPS battery time, automatically bring them back up once threat is past (lightning "flag"?)
* General alerts when some bit of expected data stops working (e.g. BLE temperature sensors stop for whatever reason).  Perhaps auto-restart NodeRED flows as an attempted fix?
* General alerts on various temperature/humidity changes.  Use trending analysis.  E.g. "it's dropped 10 degrees outside in the past hour!"
* Behavior trees for automation https://github.com/BehaviorTree/BehaviorTree.CPP


Aspects of Items to Consider
----------------------------

* Reliability - items with security controls such as garage doors should be hard wired and able to withstand network outages
* Recoverability - when power goes out, some sequence of events should occur to maximize available battery while gracefully shutting things off and notifying outside if possible.  Secondary cell radio device could function as an emergency notifier (see Aeris Neo SIM above)
* Nice GUI with cool factor like "Jarvis" from iron man.  3D displays of items would be great
* All devices used should have firmware/software versions documented and scraper to watch for updates.  IP/MAC addresses should be documented as well for easy finding
* Data storage - influx db continuous queries and data retention policies to help pare down long term data
* Everything plugged into a circuit has a circuit id tying back to the breaker map (with 3d rendering of the house) so it's easy to measure appropriate current draws or fix/install new things
* Generate a generic notification server which ties into all of the above but also other services like email and sms
* Find all the common device uses (relay boards, digital I/Os, etc) and try to come up with the cheapest common denominator device which can easily be replicated and added


Google Home Commands
--------------------

A listing of desired Google Home voice commands, e.g. "Hey Google, <command>"

* "Good night" - turn off living room lights, TV & A/V stuff, force PC screens to sleep, turn on stove night light
* "Good morning" - turn on living room lights, TV & A/V on to news, espesso machine on, turn off stove night light, bring up cameras on PC screens, read sumary of calendar items for the day, weather forecast, current outside temp (from my own data)
* "How much X money do I have?" - ties into budgeting app to read off the current amount of a given budget item
* Can google home be tied to cell phones to ring when the phone rings? (whole home phone ring?)


Architecture Considerations
---------------------------

"House as a robot" - a home automation system has all the same concepts as a robot: perception, sensors, actuators, logging, data storage, comms, power.

* ROS2 as a framework? Keep number of frameworks/platforms/middlewares down to as few as possible
* Monitors for system (e.g. computers) health, power health, network health, sensors in proper ranges, etc
* Issues such as continuing to log data during power outages, how to recover from power/computer/network outages
* https://www.structurizr.com/ - good tool for architecture


Power plan
----------

Issues which can affect power and how we recover from them

* Keep internet up and running - at the very least must power the main router and wifi AP
** If no important wifi devices are around, the AP could be shut off too.  Can we shut off PoE ports programmatically?
* Quick blips or brownouts - not really a total loss of power but could cause reboots?  Gotta check w/battery backups
* Short term loss - thunderstorm or whatever, expected to come back quickly (maybe don't have to shut everything down?)
* Long term loss - hurricane or other large weather event (freezes? come on really?) where expected to be out for days.  Can we handle that without a generator?
** Priority list of items, pending available power:
*** Fridge
*** Phone chargers
*** Lights
*** Router (cell service might be bad)
* Whole home surge protection?
* Orderly shutdown of items based on available power.  Desktop/server/monitors first to go, small arduinos/ESP32s to monitor things last (along with router).
** If internet goes down, should ESP32s shut off the router too, and turn it back on periodically to check for access?
* Can we reasonably CT clamp every circuit in the box?  Could possibly do it in the attic since they're all in one place?
