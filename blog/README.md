Bloggable Ideas
===============

* Analysis of personal information security (does it matter?)
* Driving annoyances
* I use X software and here's why it's awesome and how to use it
* How to live as ad free of a life as possible
* Annoyance at "awareness" of everything
* Fuel efficiency is more about where you live than what you drive
* Personal project success/fail blog - especially fails
* Health data tracking
* Movie plot holes
* Junk snail mail project (I kept junk mail for a year and analyzed it)
* Tipping at places like Sonic
* Product reviews
* The world's not as terrible as everyone thinks
* How to prepare for an EMP attack
* At railroad tracks, why do people stop before the tracks but not before the cross arms?
* Tour local burger restaurants, take pic of burger every time, compare with advertisements of said burger
