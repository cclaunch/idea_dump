Carputer
========

This idea has been rolling around in my head for since about 2004 or so.  Many of the thoughts and features have already been implemented as standard by carmakers, but for posterity's sake are still included.  Some of thoughts may be geared towards my 2006 Chevy Silverado specifically.

Features
--------

* OBDII/CAN vehicle data real-time
* HD/AM/FM Radio
* DVD player
* MP3 database/player
* GPS navigation/data
* Phone connection to display call data / txt messages
* Full internet access (cell wifi or city wifi if it ever happens)
* Voice recognition
* Keyfob style security
* Automatic download/update of data/logs to house comp from driveway (through house wifi)
* Convenient couple of USB ports for connecting external devices
* Rear view/other camera
* Email
* TV (via OTA?)
* Calculator
* Gas mileage gauge / oil change / inspection sticker reminder? (auto-add to google calendar/docs?)
* Work with outlook calendar   
* Transtar integration
* Weather
* Movie show times
* Computer stats (hard drive space left/battery left/mem/cpu usage)
* live sports score updates
* airline data
* garage door opener hack - software defined radio?
* radar detector built-in (legal in tx?)
* Instant "Bass" music to mess with people
* webcam(s) pointed different directions, to easily take pics
* automatically respond to texts with a "i'm driving message"... maybe toggle switch?
* "quick horn" button that will very quickly and lightly honk the horn (for getting people's attention).
* knight rider style leds on the front just for fun (probably not legal)
* auto-record radio (i.e. specific shows) then auto-cut commercials from it and allow for playback in truck
* have truck smartly detect when fuel fill-ups happen and auto-log mileage
* trouble code database which automatically shows description
* Radar gun built in to see other cars speed
* Toll road warnings
* bank/grade sensors (showing angles)
* temperature sensors in AC vents
* Camera in 3rd brake light
* Displays both spoken and text in english and spanish
* Interior temperature
* EZ tag account status lookup with amount remaining after going through toll booth
* Take weather into account on maps trips
* Compass on display
* Automatically handle logs (dropbox?)
* Display current speed limit (from maps)
* Display current street and cross streets as you approach
* Notification if fuel is significantly lower than last time truck started (e.g. fuel stolen)
* Physical button to always navigate to home
* Read tpms sensors with software defined radio
* Backwards facing dash cam? With live feeds from both dash cams to screens
* Query status of kill switch
* Quick timer/stopwatch function, to time things like traffic lights
* Battery/alternator test circuit built-in, run automatically on specified interval
* Stale data notification
* OBD data post-processor on home computer, analyze for possible issues
* “Ghost drive” previous trips to same places (similar to racing video games)
* GPS keep stats on drive to/from work
* 3D game engine usage to build in "Jarvis" style user interfaces (zooming in on various parts of the truck, etc)
* LIDAR / depth sensors scanning for obstacles
    
Plans / Hardware needs
----------------------

* Real world test runs via laptop, final implementation on SBC (single board computer)
* Could have two SBC's, one extremely low power which could stay on all the time, other which boots quickly on engine start
* Could use some type of "UPS" to keep the lower power PC online all the time
* Cell radio could report low rate simple vehicle status 24-7
* Could replace center jump seat with custom built console to house the parts
* Software simulators of various vehicle functions would be nice and aid development
* Will likely need cross compilation for various SBCs
* Keep in mind current usage and limits on the accessory circuit
* Touch screen(s) - could have multiple screens
* Expose USB ports, SD card readers, phone chargers
* Phone dock
* Space navigator style control
* Buy secondary used console from junk yard to hack up
* Space Shuttle keypad via serial port?
* Rear displays for passengers, headphones for kids
* Serial device to control all amplifier controls
* [Sensairy](http://www.sensairy.com/#/home) - pretty nice bluetooth TPMS sensor

Resources
---------

* [The GM LAN Bible](https://docs.google.com/spreadsheets/d/1qEwOXSr3bWoc2VUhpuIam236OOZxPc2hxpLUsV0xkn0)
* [OBDLink MX Bluetooth Scan Tool](https://www.scantool.net/obdlink-mxbt/)
* [Car Hacker's Handbook](http://opengarages.org/handbook/) - great resource, particularly the TPMS reading
* pids folder of this repository - lists of known good OBDII PID values
* misc folder of this repository - random files I can't relocate online
* There are listings of various car manufacturers complete PID sets, (e.g. GM has many tends of thousands) though they're very expensive. Nothing more to say about that.
