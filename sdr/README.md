Software Defined Radio
======================

Some project ideas for fun stuff to do with SDR (Software Defined Radio)

Projects
--------

* Read electric/water/gas meters
* Read TPMS sensors

Resources
---------

* [Instructable for RTL-SDR](https://www.instructables.com/id/RTL-SDR-FM-radio-receiver-with-GNU-Radio-Companion/)
* [Guided Tutorial for GNU Radio](https://wiki.gnuradio.org/index.php/Guided_Tutorial_GNU_Radio_in_Python)
* [rtlamr](https://github.com/bemasher/rtlamr) - smart meter reader
